const fs = require('fs');

const fileName = './test.json';

// fs.readFile(fileName, (err, data) => {
//     if(err){
//         console.log(err);
//     } else {
//         const result = JSON.parse(data);
//         console.log('File contents: ', result.message);
//     }
// });

try {
    const data = fs.readFileSync(fileName);
    const result = JSON.parse(data);
    console.log(result);
} catch (e) {
    console.log(e);
}
